# coding: UTF-8

from bs4 import BeautifulSoup

def parseTaskRow(row, type):
	titleTag = row.find(name = "a", attrs = {"class": "namecol normal J-worktablePreview preview-title"})
	title = titleTag["title"]
	link = titleTag["href"]
	
	hours = row.find(name = "span", attrs = {"class": "limit editable-value"}).text
	
	times = row.find_all(name = "span", attrs = {"class": "normal editable-value"})
	startTime = times[0].text.strip().replace("-", "")
	endTime = times[1].text.strip().replace("-", "")
	
	if type == "week":
		return "- [task:{}]({}) {} {}-{}".format(title, link, hours, startTime, endTime)
	else:
		return "- [{}]({}) ({}h)".format(title, link, hours)
	
def parseTasks(done, type, version):
	# Load file
	soup = BeautifulSoup(open("task.txt", "r", encoding="utf-8"), "html.parser")

	# print(soup.prettify())

	rows = soup.find_all(name = "tr", attrs = {"class": "rowdone preview" if done else "rowNOTdone preview"})
	# print(doneRows)

	result = []
	for row in rows:
		if isRowOfVersion(row, version):
			result.append(parseTaskRow(row, type))
		
	return result

def isRowOfVersion(row, version):
	if len(version) != 0:
			versionTag = row.find(name = "a", attrs = {"class": "editable-value"})

			if versionTag is not None:
				versionValue = versionTag["title"]
				if version not in versionValue:
					return False
	
	return True