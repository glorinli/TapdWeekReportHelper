# TapdWeekReportHelper

## 介绍
Tapd生成周报小助手

## 使用说明

1. 在tapd我的已办Task界面，设置显示预估工时、开始时间、结束时间字段
2. 复制页面源码，保存到python同目录下的task.txt
3. 复制BUG页面源码，保存到bug.txt
4. 运行main.py 结果输出在out.txt中

## 参数说明
-t / --type: 类型，目前支持: 1. week 周报格式   2. version 迭代总结格式

-v / --version: 迭代名称，如7.7.4
