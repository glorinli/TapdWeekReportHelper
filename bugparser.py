# coding: UTF-8

from bs4 import BeautifulSoup

def parseBugRow(row, type):
	titleTag = row.find(name = "a", attrs = {"class": "J-worktablePreview namecol preview-title"})
	title = titleTag["title"]
	link = titleTag["href"]
	
	if type == 'week':
		return "- [bugfix:{}]({})".format(title, link)
	else:
		return "- [{}]({})".format(title, link)
	
def parseBugs(type, version):
	# Load file
	soup = BeautifulSoup(open("bug.txt", "r", encoding="utf-8"), "html.parser")

	# print(soup.prettify())

	doneRows = soup.find_all(name = "tr", attrs = {"class": ["preview rowdone", "preview rowNOTdone", "preview rowNOTdone modified_item_tr"]})
	# print(doneRows)

	result = []
	for row in doneRows:
		if isRowOfVersion(row, version):
			result.append(parseBugRow(row, type))
		
	return result

def isRowOfVersion(row, version):
	if len(version) != 0:
			versionTag = row.find(name = "td", attrs = {"class": "editable-td td-editable-dropdown"})
			#print("versionTag: ", versionTag)
			if versionTag is not None:
				versionValue = versionTag["data-editable-value"]
				#print("versionValue:", versionValue)
				if version not in versionValue:
					return False
	
	return True