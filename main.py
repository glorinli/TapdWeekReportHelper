# coding: UTF-8

import taskparser
import bugparser
import getopt
import sys

supportedTypes = {"week": "周报格式", "version": "迭代总结格式"}

opt, args = getopt.getopt(sys.argv[1:], "t:v:", ["type=", "version="])

t = "week"
version = ""

for key, value in opt:
	if key in ('-t', '--type'):
		t = value
	if key in ('-v', '--version'):
		version = value

if t not in supportedTypes.keys():
	print("Unsupported type:", t)
	print("Available types:", supportedTypes)
	sys.exit(1)

print("Start parse, type: {}, version: {}".format(t, version))

# Read tasks
tasks = taskparser.parseTasks(True, t, version)
tasksUndone = taskparser.parseTasks(False, t, version)
bugs = bugparser.parseBugs(t, version)

outFile = "out_{}.txt".format(t)
out = open(outFile, "w", encoding = "utf-8")

out.write("Done Tasks")
out.write('\n')
out.write('\n')

print("Done tasks count:", len(tasks))

for t in tasks:
	out.write(t)
	out.write('\n')
	
out.write('\n')
out.write('\n')
out.write("Undone tasks")
out.write('\n')
out.write('\n')
	
print("Undone tasks count:", len(tasksUndone))

for t in tasksUndone:
	out.write(t)
	out.write('\n')
	
out.write('\n')
out.write('\n')
out.write("Bugs")
out.write('\n')
out.write('\n')

print("Bugs count:", len(bugs))
for b in bugs:
	out.write(b)
	out.write('\n')

# Close the stream
out.flush()
out.close()

print("Parse completed, out file: {}".format(outFile))

